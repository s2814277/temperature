package nl.utwente.di.temperature;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Example of a Servlet that gets an ISBN number and returns the book price
 */

public class Temperature extends HttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public void init() throws ServletException {
    }

    public void doGet(HttpServletRequest request,
        HttpServletResponse response)
        throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType = "<!DOCTYPE HTML>\n";
        String title = "Temperature";
        out.println(docType +
            "<HTML>\n" +
            "<HEAD><TITLE>" + title + "</TITLE>" +
            "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
            "</HEAD>\n" +
            "<BODY BGCOLOR=\"#FDF5E6\">\n" +
            "<H1>" + title + "</H1>\n" +
            "  <P>Celcius: " +
            request.getParameter("value") + "\n" +
            "  <P>Fahrenheit: " +
            getFahrenheit(Double.parseDouble(request.getParameter("value"))) +
            "</BODY></HTML>");
    }

    public double getFahrenheit(double celsius) {
        return celsius * 9.0 / 5.0 + 32.0;
    }

}
